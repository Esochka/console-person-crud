import com.company.*;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class HelperTest {

    private PersonStorage personStorage;
    private File file;

    @Before
    public void setUp() {
        List<Person> personList = new ArrayList<>();
        Person person = new Person(1, "Vlad",
                "Lutsenko", 20, "Kyiv");
        personList.add(person);
        personStorage = new PersonStorage(personList);
    }
    @Before
    public void setUp2() {
        List<Person> personList = new ArrayList<>();
        Person person = new Person(1, "Vlad",
                "Lutsenko", 20, "Kyiv");
        personList.add(person);
        personStorage = new PersonStorage(personList);
        FileUtil.writeFile(new File("jsTest2.json"), false, Parser.toJSON(personStorage));
    }

    @Test
    public void getByIdTest() {

        List<Person> expected = new ArrayList<>();

        expected.add(new Person(0, "1", "2", 3, "4"));
        PersonStorage personStorage = new PersonStorage(expected);
        Person actual = Helper.getById(0, personStorage);
        assertEquals(expected.get(0), actual);
    }

    @Test
    public void getAllByCityTest() {

        List<Person> expected = new ArrayList<>();

        expected.add(new Person(0, "1", "2", 3, "4"));
        PersonStorage personStorage = new PersonStorage(expected);
        List<Person> actual = Helper.getAllByCity("4", personStorage);
        assertEquals(expected, actual);
    }

    @Test
    public void getAllByAgeTest() {

        List<Person> expected = new ArrayList<>();

        expected.add(new Person(0, "1", "2", 3, "4"));
        PersonStorage personStorage = new PersonStorage(expected);
        List<Person> actual = Helper.getAllByAge(3, personStorage);
        assertEquals(expected, actual);
    }

    @Test
    public void getAllByFirstNameTest() {

        List<Person> expected = new ArrayList<>();

        expected.add(new Person(0, "1", "2", 3, "4"));
        PersonStorage personStorage = new PersonStorage(expected);
        List<Person> actual = Helper.getAllByFirstName("1", personStorage);
        assertEquals(expected, actual);
    }

    @Test
    public void getAllByLastNameTest() {

        List<Person> expected = new ArrayList<>();

        expected.add(new Person(0, "1", "2", 3, "4"));
        PersonStorage personStorage = new PersonStorage(expected);
        List<Person> actual = Helper.getAllByLastName("2", personStorage);
        assertEquals(expected, actual);

    }

    @Test
    public void deleteAllTest() {

        Helper.deleteAll(this.personStorage );
        assertEquals(this.personStorage.getPersons().size(), 0);

    }
}
