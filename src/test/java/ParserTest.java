import com.company.FileUtil;
import com.company.Parser;
import com.company.Person;
import com.company.PersonStorage;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ParserTest {
    private List<Person> personList = new ArrayList<>();

    @Before
    public void setUp() {
        Person person = new Person(1, "Vlad", "Lutsenko", 20, "Kyiv");
        personList.add(person);
    }

    @Test
    public void toJSONTest() {

        PersonStorage personStorage = new PersonStorage(personList);

        String expected = "[\n" +
                "{\n" +
                " \"id\":1,\n" +
                " \"firstName\":\"Vlad\",\n" +
                " \"lastName\":\"Lutsenko\",\n" +
                " \"age\":20,\n" +
                " \"city\":\"Kyiv\" \n" +
                " }\n" +
                "]";

        String result = Parser.toJSON(personStorage);
        assertEquals(expected, result);
    }

    @Test
    public void toXmlTest() {

        PersonStorage personStorage = new PersonStorage(personList);
        String expected = "<Persons>\n" +
                " <Person>\n" +
                " <id>1</id>\n" +
                " <firstName>Vlad</firstName>\n" +
                " <lastName>Lutsenko</lastName>\n" +
                " <age>20</age>\n" +
                " <city>Kyiv</city>\n" +
                " </Person>\n" +
                "</Persons>\n";

        String result = Parser.toXml(personStorage);
        assertEquals(expected, result);
    }

    @Test
    public void toCvsTest() {

        PersonStorage personStorage = new PersonStorage(personList);
        String expected = "id,firstName,lastName,age,city \n" +
                "1,Vlad,Lutsenko,20,Kyiv \n";

        String result = Parser.toCsv(personStorage);
        assertEquals(expected, result);
    }

    @Test
    public void toYamlTest() {

        PersonStorage personStorage = new PersonStorage(personList);
        String expected = "---\n" +
                "- \"id:1, firstName:Vlad,lastName:Lutsenko,age:20,city:Kyiv\" \n";

        String result = Parser.toYaml(personStorage);
        assertEquals(expected, result);
    }

    @Test
    public void getAllObjectsTest() {

        List<Person> data = new ArrayList<>();
        data.add(new Person(0, "1", "2", 3, "4"));
        List<Person> actual = Parser.getAllObjects("./fileTest/jsTest.json");
        assertEquals(data, actual);
    }

    @Test
    public void updatePersonTest() {

        List<Person> expected = new ArrayList<>();
        expected.add(new Person(0, "1", "2", 3, "4"));
        PersonStorage personStorage = new PersonStorage(expected);
        Parser.updatePerson(0, "3", "3", 3, "3", personStorage);

        List<Person> actual = new ArrayList<>();
        actual.add(new Person(0, "3", "3", 3, "3"));
        assertEquals(expected, actual);
    }

    @Test
    public void deleteByIdTest() {

        List<Person> expected = new ArrayList<>();
        expected.add(new Person(0, "1", "2", 3, "4"));
        PersonStorage personStorage = new PersonStorage(expected);
        Parser.deleteById(0, personStorage);

        List<Person> actual = new ArrayList<>();
        assertEquals(personStorage.getPersons(), actual);
    }
}
