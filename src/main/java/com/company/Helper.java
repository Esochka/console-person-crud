package com.company;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Helper {

    public static Person getById(Integer id, PersonStorage personStorage) {
        List<Person> personList = personStorage.getPersons();
        for (Person person : personList) {
            if (person.getId().equals(id)) {
                return person;
            }
        }
        return null;
    }

    public static List<Person> getAllByCity(String city, PersonStorage personStorage) {
        List<Person> personList = personStorage.getPersons();
        List<Person> resultList = new ArrayList<>();
        for (Person person : personList) {
            if (person.getCity().equals(city)) {
                resultList.add(person);
            }
        }
        return resultList;
    }

    public static List<Person> getAllByAge(Integer age, PersonStorage personStorage) {
        List<Person> personList = personStorage.getPersons();
        List<Person> resultList = new ArrayList<>();
        for (Person person : personList) {
            if (person.getAge().equals(age)) {
                resultList.add(person);
            }
        }
        return resultList;
    }

    public static List<Person> getAllByFirstName(String firstName, PersonStorage personStorage) {
        List<Person> personList = personStorage.getPersons();
        List<Person> resultList = new ArrayList<>();
        for (Person person : personList) {
            if (person.getFirstName().equals(firstName)) {
                resultList.add(person);
            }
        }
        return resultList;
    }

    public static List<Person> getAllByLastName(String lastName, PersonStorage personStorage) {
        List<Person> personList = personStorage.getPersons();
        List<Person> resultList = new ArrayList<>();
        for (Person person : personList) {
            if (person.getLastName().equals(lastName)) {
                resultList.add(person);
            }
        }
        if (resultList.isEmpty()) {
            System.out.println("No persons found");
        }
        return resultList;
    }

    public static void deleteAll(PersonStorage personStorage ) {
        List<Person> personList = personStorage.getPersons();
        personList.clear();
    }
}
