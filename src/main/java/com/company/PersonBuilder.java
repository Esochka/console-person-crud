package com.company;

import com.company.Person;
import com.company.PersonStorage;

import java.io.File;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class PersonBuilder {


    private static Scanner scanner = new Scanner(System.in);

    public static Person createPerson(PersonStorage personStorage,
                                      String firstNameScan,
                                      String lastNameScan,
                                      Integer ageScan, String cityScan) {
        Integer id = 0;
        List<Person> personList = personStorage.getPersons();
        if (personList.isEmpty()) {
            id = 0;
        } else {
            id = personList.stream().max((o1, o2) -> {
                if (o1.getId() > o2.getId()) {
                    return 1;
                } else if (o1.getId() < o2.getId()) {
                    return -1;
                } else return 0;
            }).get().getId();
            id++;
        }
        System.out.println("Your id=" + id + ")");
        return new Person(id, firstNameScan, lastNameScan, ageScan, cityScan);
    }
}




