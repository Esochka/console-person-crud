package com.company;

import java.io.File;
import java.util.Scanner;

public class Menu {

    private static Scanner scanner = new Scanner(System.in);
    private static PersonStorage personStorage;


    public static void showMenu() {
        boolean flagfor = true;
        while (flagfor) {
            System.out.print("Select a file type: \nEnter 1 for JSON " +
                    "\nEnter 2 for XML \nEnter 3 for CSV " +
                    "\nEnter 4 for YAML \n ");

            String typeOfFile = scanner.next();
            boolean flag = true;
            String ac = null;
            try {

                if (typeOfFile.equals("1")) {
                    if(!new File("js.json").isFile())
                    {
                        File f = new File("js.json");
                        
                        if (f.createNewFile())
                            System.out.println("File created");
                        else
                            System.out.println("File already exists");
                    }
                    personStorage = new PersonStorage(Parser.getAllObjects("js.json"));

                    while (flag) {
                        System.out.print("Select action: \nEnter 1 for CRUD " +
                                "\nEnter 2 for HELPERS \n ");
                        String typeOfAction = scanner.next();

                        if (typeOfAction.equals("1")) {
                            System.out.print("Select action: \nEnter 1 for Create " +
                                    "\nEnter 2 for ReadAll \nEnter 3 for Update " +
                                    "\nEnter 4 for Delete \n ");
                            String action = scanner.next();
                            crudMenu(action, "js.json");
                        } else if (typeOfAction.equals("2")) {
                            System.out.print("Select action: \nEnter 1 for getById " +
                                    "\nEnter 2 for getAllByCity " +
                                    "\nEnter 3 for getAllByAge" +
                                    "\nEnter 4 for getAllByFirstName " +
                                    "\nEnter 5 for getAllByLastName " +
                                    "\nEnter 6 for deleteAll \n ");
                            String action = scanner.next();
                            helperMenu(action, personStorage,
                                    getFileNameByTypeOfFile(typeOfFile));
                        }
                        System.out.println("Enter 1 for continue or 2 for exit");
                        ac = new Scanner(System.in).next();
                        if (ac.equals("2")) {
                            flag = false;
                        }
                    }
                } else if (typeOfFile.equals("2")) {

                    if(!new File("xm.xml").isFile())
                    {
                        File f = new File("xm.xml");
                       
                        if (f.createNewFile())
                            System.out.println("File created");
                        else
                            System.out.println("File already exists");
                    }

                    personStorage = new PersonStorage(Parser.getAllObjects("xm.xml"));

                    while (flag) {
                        System.out.print("Select action: \nEnter 1 for CRUD " +
                                "\nEnter 2 for HELPERS \n ");

                        String typeOfAction = scanner.next();
                        if (typeOfAction.equals("1")) {
                            System.out.print("Select action: \nEnter 1 for Create " +
                                    "\nEnter 2 for ReadAll \nEnter 3 for Update " +
                                    "\nEnter 4 for Delete \n ");
                            String action = scanner.next();
                            crudMenu(action, "xm.xml");
                        } else if (typeOfAction.equals("2")) {
                            System.out.print("Select action: \nEnter 1 for getById " +
                                    "\nEnter 2 for getAllByCity " +
                                    "\nEnter 3 for getAllByAge" +
                                    "\nEnter 4 for getAllByFirstName " +
                                    "\nEnter 5 for getAllByLastName " +
                                    "\nEnter 6 for deleteAll \n ");
                            String action = scanner.next();
                            helperMenu(action, personStorage,
                                    getFileNameByTypeOfFile(typeOfFile));
                        }
                        System.out.println("Enter 1 for continue or 2 for exit");
                        ac = new Scanner(System.in).next();
                        if (ac.equals("2")) {
                            flag = false;
                        }
                    }
                } else if (typeOfFile.equals("3")) {

                    if(!new File("cv.csv").isFile())
                    {
                        File f = new File("cv.csv");
                       
                        if (f.createNewFile())
                            System.out.println("File created");
                        else
                            System.out.println("File already exists");
                    }

                    personStorage = new PersonStorage(Parser.getAllObjects("cv.csv"));
                    while (flag) {
                        System.out.print("Select action: \nEnter 1 for CRUD " +
                                "\nEnter 2 for HELPERS \n ");
                        String typeOfAction = scanner.next();
                        if (typeOfAction.equals("1")) {
                            System.out.print("Select action: \nEnter 1 for Create " +
                                    "\nEnter 2 for ReadAll \nEnter 3 for Update " +
                                    "\nEnter 4 for Delete \n ");
                            String action = scanner.next();
                            crudMenu(action, "cv.csv");
                        } else if (typeOfAction.equals("2")) {
                            System.out.print("Select action: \nEnter 1 for getById " +
                                    "\nEnter 2 for getAllByCity " +
                                    "\nEnter 3 for getAllByAge" +
                                    "\nEnter 4 for getAllByFirstName " +
                                    "\nEnter 5 for getAllByLastName " +
                                    "\nEnter 6 for deleteAll \n ");
                            String action = scanner.next();
                            helperMenu(action, personStorage,
                                    getFileNameByTypeOfFile(typeOfFile));
                        }
                        System.out.println("Enter 1 for continue or 2 for exit");
                        ac = new Scanner(System.in).next();
                        if (ac.equals("2")) {
                            flag = false;
                        }
                    }
                } else if (typeOfFile.equals("4")) {

                    if(!new File("ya.yaml").isFile())
                    {
                        File f = new File("ya.yaml");
                       
                        if (f.createNewFile())
                            System.out.println("File created");
                        else
                            System.out.println("File already exists");
                    }

                    personStorage = new PersonStorage(Parser.getAllObjects("ya.yaml"));

                    while (flag) {
                        System.out.print("Select action: \nEnter 1 for CRUD " +
                                "\nEnter 2 for HELPERS \n ");

                        String typeOfAction = scanner.next();
                        if (typeOfAction.equals("1")) {
                            System.out.print("Select action: \nEnter 1 for Create " +
                                    "\nEnter 2 for ReadAll \nEnter 3 for Update " +
                                    "\nEnter 4 for Delete \n ");
                            String action = scanner.next();
                            crudMenu(action, "ya.yaml");
                        } else if (typeOfAction.equals("2")) {
                            System.out.print("Select action: \nEnter 1 for getById " +
                                    "\nEnter 2 for getAllByCity " +
                                    "\nEnter 3 for getAllByAge" +
                                    "\nEnter 4 for getAllByFirstName " +
                                    "\nEnter 5 for getAllByLastName " +
                                    "\nEnter 6 for deleteAll \n ");
                            String action = scanner.next();
                            helperMenu(action, personStorage,
                                    getFileNameByTypeOfFile(typeOfFile));
                        }
                        System.out.println("Enter 1 for continue or 2 for exit");
                        ac = new Scanner(System.in).next();
                        if (ac.equals("2")) {
                            flag = false;

                        }
                    }
                } else {
                    continue;
                }

                if (ac.equals("2")) {
                    flagfor = false;
                    safeForFile(typeOfFile);
                }
            } catch (Exception e) {

                System.out.println("Error) data is written to file");
                safeForFile(typeOfFile);
            }
        }


    }


    private static String getFileNameByTypeOfFile(String typeOfFile) {
        if (typeOfFile.equals("1")) {
            return "js.json";
        } else if (typeOfFile.equals("2")) {
            return "xm.xml";
        } else if (typeOfFile.equals("3")) {
            return "cv.csv";
        } else if (typeOfFile.equals("4")) {
            return "ya.yaml";
        }
        return "";
    }

    private static void helperMenu(String action, PersonStorage personStorage,
                                   String fileName) {
        switch (action) {
            case "1": {
                System.out.println("Enter Id ");
                Integer idHelpers = scanner.nextInt();
                System.out.println(Helper.getById(idHelpers, personStorage));
                break;
            }
            case "2": {
                System.out.println("Enter City ");
                String cityHelpers = scanner.next();
                System.out.println(Helper.getAllByCity(cityHelpers, personStorage));
                break;
            }
            case "3": {
                System.out.println("Enter Age ");
                Integer ageHelpers = scanner.nextInt();
                System.out.println(Helper.getAllByAge(ageHelpers, personStorage));
                break;
            }
            case "4": {
                System.out.println("Enter FirstName ");
                String firstNameHelpers = scanner.next();
                System.out.println(Helper.getAllByFirstName(firstNameHelpers, personStorage));
                break;
            }

            case "5": {
                System.out.println("Enter LastName ");
                String secondHelpers = scanner.next();
                System.out.println(Helper.getAllByLastName(secondHelpers, personStorage));
                break;
            }
            case "6": {
                Helper.deleteAll(personStorage);
                safeForFile(fileName);
                break;
            }
        }
    }

    private static void crudMenu(String action, String fileName) {
        switch (action) {
            case "1": {
                System.out.print("Enter your firstName: ");
                String firstNameScan = scanner.next();

                System.out.print("Enter your lastName: ");
                String lastNameScan = scanner.next();

                System.out.print("Enter your age: ");
                while (!scanner.hasNextInt()) {
                    System.out.println("That not a number! Enter your age:");
                    scanner.next();
                }
                Integer ageScan = scanner.nextInt();


                System.out.print("Enter your city: ");
                String cityScan = scanner.next();

                if (fileName.endsWith(".json")) {
                    personStorage.getPersons().add(PersonBuilder.createPerson(personStorage, firstNameScan, lastNameScan, ageScan, cityScan));
                    FileUtil.writeFile(new File("js.json"), false, Parser.toJSON(personStorage));
                } else if (fileName.endsWith(".xml")) {
                    personStorage.getPersons().add(PersonBuilder.createPerson(personStorage, firstNameScan, lastNameScan, ageScan, cityScan));
                    FileUtil.writeFile(new File("xm.xml"), false, Parser.toXml(personStorage));
                } else if (fileName.endsWith(".csv")) {
                    personStorage.getPersons().add(PersonBuilder.createPerson(personStorage, firstNameScan, lastNameScan, ageScan, cityScan));
                    FileUtil.writeFile(new File("cv.csv"), false, Parser.toCsv(personStorage));
                } else if (fileName.endsWith(".yaml")) {
                    personStorage.getPersons().add(PersonBuilder.createPerson(personStorage, firstNameScan, lastNameScan, ageScan, cityScan));
                    FileUtil.writeFile(new File("ya.yaml"), false, Parser.toYaml(personStorage));
                }
                break;
            }
            case "2": {
                System.out.println(personStorage.getPersons());
                break;
            }
            case "3": {
                System.out.println("Enter Id for update");
                Integer id = scanner.nextInt();
                Person person = Helper.getById(id, personStorage);
                if (person == null) {
                    System.out.println("Person with this id doesn't exist");
                } else {
                    System.out.println(person);

                    System.out.print("Enter new firstName: ");
                    String firstNameScan = scanner.next();

                    System.out.print("Enter new lastName: ");
                    String lastNameScan = scanner.next();

                    System.out.print("Enter new age: ");
                    while (!scanner.hasNextInt()) {
                        System.out.println("That not a number! Enter your age:");
                        scanner.next();
                    }
                    Integer ageScan = scanner.nextInt();

                    System.out.print("Enter new city: ");
                    String cityScan = scanner.next();

                    Parser.updatePerson(id, firstNameScan, lastNameScan, ageScan, cityScan, personStorage);

                    safeForFile(fileName);

                }
                break;
            }
            case "4": {
                System.out.println("Enter Id for delete");
                Integer id = scanner.nextInt();
                Parser.deleteById(id, personStorage);
                safeForFile(fileName);
                break;
            }
            default: {
                System.out.println("Wrong data");
            }
        }
    }


    private static void safeForFile(String fileName) {
        if (fileName.endsWith(".json")) {
            FileUtil.writeFile(new File("js.json"), false, Parser.toJSON(personStorage));

        } else if (fileName.endsWith(".xml")) {
            FileUtil.writeFile(new File("xm.xml"), false, Parser.toXml(personStorage));

        } else if (fileName.endsWith(".csv")) {
            FileUtil.writeFile(new File("cv.csv"), false, Parser.toCsv(personStorage));

        } else if (fileName.endsWith(".yaml")) {
            FileUtil.writeFile(new File("ya.yaml"), false, Parser.toYaml(personStorage));

        }
    }


}
